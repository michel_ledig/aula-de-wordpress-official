<?php
    function add_styles_and_scripts(){ 
        wp_enqueue_style( "reset-sheet", get_template_directory_uri() . "/assets/css/reset.css");
        wp_enqueue_style( "style-sheet", get_template_directory_uri() . "/style.css");
        wp_enqueue_script("mail-go-sriptt", get_template_directory_uri() . "assets/js/mailgo.min.js");
    }
    add_action( 'wp_enqueue_scripts', 'add_styles_and_scripts' );

?>